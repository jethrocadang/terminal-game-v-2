# FUTURE NINTENDO 2.0

# About the Project

  This project is a partial fullfilment for the finals of first semester 2022-2023 in Data Structures and Algorithms.

  The project is created using python language. It is implemented using Object Oriented Programming (OOP). The project is made up of three games SNAKE, FOUR IN A ROW, and SUDUKO. 

# About the Authors 

  The authors of this project are Jethro R Cadang and Royye Agustin. 

  Jethro Cadang is aspiring software engineering or website developer.

  Royye Agustin is apiring website developer.

# Installation Guidelines

  1. Clone the github file.

  2. Run python3 authentication.py on your terminal.

  3. In the main menu you can select the choices you want.

# Usage Guide 

  1. In game menu create account in REGISTER.

  2. LOG IN using registered account.

  3. In MAIN MENU you can start to select yout desired choices 
     by typing your INPUT.
  4. To exit the Game press exit.